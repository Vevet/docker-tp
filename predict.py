import numpy as np
from joblib import load

# Importing the model
knn = load('knnmodel.jolib')

# Asking for parameters
print('Enter your parameters : ')
sl = input('sepal length : ')
sw = input('sepal width : ')
pl = input('petal length : ')
pw = input('petal width : ')
parameters = [sl, sw, pl, pw]

# Predicting the variety
prediction = np.array([parameters]).astype(np.float64)
print(knn.predict(prediction))