## Téléchargement du projet
    - Télécharger le projet git et le dézipper dans un dossier à part ou le cloner.

## Création du container docker
    - Lancer une invite de commande et aller dans le path du dossier où se trouvent les fichiers.
    - Lancer la commande `docker build -t <image-name> .` pour créer le container docker et lancer l'entrainement du modèle.

## Lancement du container docker
    - Lancer la commande `docker run -it <image-name>` pour lancer le container et pouvoir interagir avec le modèle.
    - Si vous voulez réentrainer le modèle écrire dans l'invite de commande : `python3 train.py`.
    - Si vous voulez prédir la variété écrire dans l'invite de commande : `python3 predict.py` et saisir les différentes valeurs demandées.
