FROM python:3.7.9-slim


COPY requirements.txt /
RUN pip3 install -r /requirements.txt

COPY . /app

WORKDIR /app

RUN ["python3", "train.py"]

ENTRYPOINT [ "bash" ]
