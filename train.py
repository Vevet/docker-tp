import pandas as pd
from connect import Connect
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from joblib import dump

# Connection to the database
client = Connect.get_connection()

# Retrieval of the dataset
db = client['uppa']
iris = db.iris
iris_df = pd.DataFrame(iris.find())
iris_data = iris_df[['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'variety']]

# Removal of the variety for the training
x = iris_data.drop(['variety'], axis=1)
y = iris_data['variety']

# Spliting the dataset
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)

# Training the model
knn = KNeighborsClassifier(n_neighbors=8)
knn.fit(x_train, y_train)

# Exporting the model
dump(knn, 'knnmodel.jolib')