from pymongo import MongoClient
import certifi

class Connect(object):
    @staticmethod    
    def get_connection(CONNECTION_STRING='mongodb+srv://estia2022:estia2022@hupimongocluster.rxgzj.mongodb.net/test'):
        return MongoClient(CONNECTION_STRING, tlsCAFile=certifi.where())